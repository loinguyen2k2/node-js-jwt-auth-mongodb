const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");
const db = require("../models");
const User = db.user;
const Role = db.role;

const verifyToken = (req, res, next) => {
  let token = req.headers["x-access-token"];
  if (!token) {
    return res.status(403).send({ message: "No token provided!" });
  }
  
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({ message: "Unauthorized!" });
    }
    req.userId = decoded.id;
    next();
  });
};

const checkUserRole = (req, res, next, roleName) => {
  User.findById(req.userId).exec((err, user) => {
    if (err) {
      return res.status(500).send({ message: err });
    }
    Role.find(
      {
        _id: { $in: user.roles }
      },
      (err, roles) => {
        if (err) {
          return res.status(500).send({ message: err });
        }
        for (let i = 0; i < roles.length; i++) {
          if (roles[i].name === roleName) {
            next();
            return;
          }
        }
        res.status(403).send({ message: `Require ${roleName} Role!` });
        return;
      }
    );
  });
};

const isAdmin = (req, res, next) => {
  checkUserRole(req, res, next, "admin");
};

const isModerator = (req, res, next) => {
  checkUserRole(req, res, next, "moderator");
};

const authJwt = {
  verifyToken,
  isAdmin,
  isModerator
};

module.exports = authJwt;
